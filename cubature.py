import numpy as np
from utilities import cart, outer, leja_order, leg, leg_bary, lob, barycentric


class Cubature:

    def __init__(self, deg=[100], points="Legendre"):
        """
        This is the Initialisation of the Cubature Class.

        :param deg: a list, which contains possibly multiple distinct degrees.
        :param points: either "Legendre" or "Lobatto". Legendre points do not contain the endpoints "-1", "+1", but Lobatto points do.
        """
        self.dim = len(deg)
        self.deg = deg
        self.points = points
        self.axes, self.axes_weights, self.axes_leja = self._axes()
        self.leja_axes, self.leja_axes_weights = self._leja_axes()
        self.grid = cart(self.axes)
        self.leja_grid = cart(self.leja_axes)
        self.leja_weights = outer(self.leja_axes_weights)
        self.leja_axes_bary = self._leja_axes_bary()

    def _axes(self):
        """
        Initialisation of axes data.

        :return:
            all different axes data types, including axes: a list where each element is an axis and consists of points
            betweeen -1 and +1; axes_weights: the weights for the quadrature formula on the corresponding axis;
            axes_leja, technically the same as axes, but in a different order, which is the most efficient one to
            compute with.
        """

        # Init with the empty list
        axes = []
        axes_weights = []
        axes_leja = []

        # Enumerate all different degrees.
        for i, _ in enumerate(self.deg):

            # If the axis was already computed for this specific degree, then copy the already computed one.
            if _ in self.deg[0:i]:
                j = self.deg.index(_)

                axis = axes[j]
                axis_weight = axes_weights[j]
                axis_leja = axes_leja[j]

            # If not, compute the axis and corresponding weights.
            else:
                axis = None

                # Check which point set should be computed.
                if self.points == "Legendre":
                    axis, axis_weight = leg(_)
                elif self.points == "Lobatto":
                    axis, axis_weight = lob(_)

                axis_leja = leja_order(axis)

            # Append all three elements to their corresponding and predefined lists.
            axes.append(axis)
            axes_weights.append(axis_weight)
            axes_leja.append(axis_leja)

        return axes, axes_weights, axes_leja

    def _leja_axes_bary(self):
        """
        Initialisation of barycentric weights on leja ordered axes.

        :return: all barycentric weights with respect to the axes data.
        """

        # Init with the empty list
        leja_axes_bary = []

        # Enumerate all different degrees.
        for i, _ in enumerate(self.deg):
            # If the barycentrics were already computed for this specific degree, then copy the already computed one.
            if _ in self.deg[0:i]:
                j = self.deg.index(_)
                leja_axis_bary = leja_axes_bary[j]

            # If not, compute the barycentrics
            else:
                leja_axis_bary = None

                # Check which point set should be computed.
                if self.points == "Legendre":
                    leja_axis_bary = leg_bary(self.axes[i], self.axes_weights[i])[self.axes_leja[i]]
                elif self.points == "Lobatto":
                    leja_axis_bary = np.array(barycentric(self.axes[i]))[self.axes_leja[i]]

            # Append to the predefined list.
            leja_axes_bary.append(leja_axis_bary)

        return leja_axes_bary

    def _leja_axes(self):
        """
        Initialisation of leja ordered axes/weights. This method simply applies the order to the non-leja-ordered
        axes data and weights.

        :return: leja ordered axes data and weights.
        """
        return [self.axes[_][self.axes_leja[_]] for _ in range(self.dim)], \
               [self.axes_weights[_][self.axes_leja[_]] for _ in range(self.dim)]