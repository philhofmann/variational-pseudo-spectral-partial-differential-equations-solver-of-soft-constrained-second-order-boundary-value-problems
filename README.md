# Variational Pseudo-Spectral Partial Differential Equations Solver of Soft-Constrained Second Order Boundary Value Problems



## Getting started

Welcome! This is a framework for solving soft-constrained second order boundary values problems. Clone the project and have a look at _"docs/build/html/index.html"_ for the documentation, as well as all numerical examples.


## Contact

Phil Hofmann <philhofmann@outlook.com>


## Acknowledgements

Many thanks to Dr. Michael Hecht and Juan Esteban Suarez.