import numpy as np
import scipy as sp
import scipy.special as sps
import itertools
from functools import partial
import torch
from scipy.special import eval_chebyt, roots_legendre

# UTILITIES FOR BASIC ARITHMETICS #


def bisection(f, a, b, tol=1e-14):
    """
    Bisection method.

    :param f: objective function f.
    :param a: left end point of initial interval.
    :param b: right end point of initial interval.
    :param tol: tolerance for the determination of the algorithm.
    :return: approximate solution to f(x) = 0.
    """
    assert np.sign(f(a)) != np.sign(f(b))
    m = 0
    while b - a > tol:
        m = a + (b - a) / 2
        fm = f(m)
        if np.sign(f(a)) != np.sign(fm):
            b = m
        else:
            a = m
    return m


def inverse_lu(m):
    """
    Inversion of Matrix via LU-Decomposition.

    First compute the LU-Decomposition, then invert the matrices separately and after that compose them again to obtain
    the inverse matrix.

    :param m: a matrix.
    :return: the inverse of the matrix m.
    """
    p, l, u = sp.linalg.lu(m)

    p_inv = np.linalg.inv(p)
    l_inv = np.linalg.inv(l)
    u_inv = np.linalg.inv(u)

    return matmul(u_inv, l_inv, p_inv)


def powers(mat, k):
    """
    Computation of Potencies of a given Matrix up to order k.

    :param mat: a matrix.
    :param k: until which exponent the potencies should be computed.
    :return: a complete list of potencies mat^j for j=1,2,...,k.
    """
    n, m = mat.shape
    if n == m:
        pot = np.zeros((k + 1, n, n))
        pot[0] = np.eye(n)
        for i in range(k):
            pot[i + 1] = np.matmul(pot[i], mat)
        return pot
    else:
        return None


def matmul(*mats):
    """
    Matrix product:
        * assume M1, M2 matrices.
        * assume M1 has m1 columns and M2 has m2 rows.
        * if m1 == m2     => np.matmul or torch.matmul.
        * if m1 % m2 == 0 => extend M2 to a block diagonal, such that it becomes feasible => np.matmul or torch.matmul.
        * if m2 % m1 == 0 => use np.matmul or torch.matmul for every m1-th rows of M2.

    :param mats: a list of matrices.
    :return: their product, in a modified non-convential way.
    """
    is_tensor = torch.is_tensor(mats[-1])
    is_array = bool(1-np.isscalar(mats[-1]))

    if not(is_tensor|is_array):
        return None

    n = len(mats)
    r = mats[-1]
    for _ in np.arange(1, n):
        m = mats[n-1-_]

        if (not torch.is_tensor(m)) & is_tensor:
            m = torch.tensor(m)
        elif np.isscalar(m) & is_array:
            m = np.array(m)

        len_m = m.shape[len(m.shape)-1]
        len_r = len(r)
        q = int(len_r/len_m)
        s = int(len_m/len_r)
        if len_r == len_m:
            if is_tensor:
                r = torch.matmul(m, r)
            elif is_array:
                r = np.matmul(m, r)
        elif len_r % len_m == 0:
            if is_tensor:
                r = torch.cat([torch.matmul(m, r[_*len_m:(_+1)*len_m]) for _ in range(q)])
            elif is_array:
                r = np.concatenate([np.matmul(m, r[_*len_m:(_+1)*len_m]) for _ in range(q)])
        elif len_m % len_r == 0:
            if is_tensor:
                r = torch.matmul(m, torch.block_diag(*([r] * s)))
            elif is_array:
                r = np.matmul(m, sp.linalg.block_diag(*([r]*s)))
        else:
            return None

    return r


def cart(xs, flip=True):
    """
    Cartesian Product.

    :param xs: a list of lists of points of the same length.
    :param flip: boolean, if the order should be flipped or not.
    :return: cartesian product of the elements containing xs either flipped or not.
    """
    if flip:
        xs = np.flip(xs, 0)
        return np.array(np.flip([_ for _ in itertools.product(*xs)], 1))
    else:
        return np.array([_ for _ in itertools.product(*xs)])


def cart_m(x, m, flip=True):
    """
    Cartesian Product.

    :param x: a list of points.
    :param m: the dimension.
    :param flip: boolean, if the order should be flipped or not.
    :return: cartesian product of m times x either flipped or not.
    """
    prep = np.array([x for _ in range(m)])
    return cart(prep, flip)


def outer(ws):
    """
    Extension of outer product - backward direction.

    :param ws: list of lists of points.
    :return: the composition of outer products of the list ws.
    """
    res = ws[-1]
    for _ in range(len(ws)-1):
        res = np.outer(res, ws[len(ws)-_-2]).reshape(-1)
    return res


def outer_arr(arr):
    """
    Extension of outer product - forward direction.

    :param arr: list of lists of points.
    :return: the composition of outer products of the list arr.
    """
    n = len(arr)
    res = np.array([1])
    for _ in range(n):
        res = np.outer(arr[n - (_ + 1)], res).reshape(-1)
    return res


def mui_lp(m, n, p):
    """
    Computes the multi-index set A_{m,n,p}.

    :param m: dimension.
    :param n: degree.
    :param p: p for the l^p-norm.
    :return:
    """
    return np.array([_ for _ in cart_m(np.arange(n+1), m) if np.linalg.norm(_, p) <= n])


# UTILITIES FOR CUBATURE AND DIFFERENTIATION #


def diff_mat(nodes, w):
    """
    Computation of one dimensional Differentiation Matrix.

    :param nodes: point set.
    :param w: barycentrics.
    :return: differentiation matrix.
    """
    n = len(nodes)
    dx = np.zeros((n, n))
    for i in range(n):
        for j in range(n):
            if i == j:
                dx[i][i] = np.sum(
                    [1 / (nodes[i] - nodes[_])
                     for _ in range(n) if _ != i])
            else:
                dx[i][j] = (w[j] / w[i]) * 1 / (nodes[i] - nodes[j])
    return dx


def leg(deg):
    """
    Legendre Points and Weights.

    :param deg: degree.
    :return: Roots of Legendre polynomial of degree deg+1 and the corresponding quadrature weights.
    """
    axis, axis_weight = roots_legendre(deg+1)
    return axis, axis_weight


def leg_bary(axis, axis_weight):
    """
    Computation of Barycentric Weights on Legendre Point Set.

    :param axis: Legendre point set.
    :param axis_weight: Quadrature weights.
    :return: Barycentric weights.
    """
    return np.array([(-1)**_ for _ in range(len(axis))])*np.sqrt((1-axis**2)*axis_weight)


def eval_leg_deriv(n, x):
    """
    Evaluation of the Derivative of the Legendre Polynomial of Degree n.

    :param n: degree of Legendre Polynomial.
    :param x: point of evaluation.
    :return: function value.
    """
    return (x*sps.eval_legendre(n, x) - sps.eval_legendre(n-1, x))/((x**2-1)/n)


def lob(deg):
    """
    Lobatto Points and Weights.

    :param deg: degree.
    :return:
        Union of the roots of the Legendre polynomial of degree deg-1 with the endpoints -1 and +1, as well as the
        corresponding quadrature weights.
    """
    n = deg+1
    brackets = sps.legendre(n-1).weights[:, 0]
    axis = np.zeros(n)
    axis[0] = -1
    axis[-1] = 1

    for _ in range(n-2):
        axis[_+1] = bisection(
            partial(eval_leg_deriv, n-1),
            brackets[_], brackets[_+1])

    axis_weight = np.zeros(n)
    axis_weight[0] = 2/n/(n-1)
    axis_weight[-1] = 2/n/(n-1)

    for _ in range(n-2):
        axis_weight[_+1] = 2/n/(n-1)/(sps.eval_legendre(n-1, axis[_+1]))**2

    return axis, axis_weight


def barycentric(nodes):
    """
    Computation of Barycentric Weights on arbitrary Point Set.

    :param nodes: point set.
    :return: Barycentric weights.
    """
    return [1/np.prod([
        (nodes[__] - nodes[_]) for _ in range(len(nodes)) if _ != __
    ]) for __ in range(len(nodes))]


# UTILITIES IN ORDER TO REDUCE NUMERICAL ERROR #


def leja_order(nodes):
    """
    Leja Order of Nodes.

    :param nodes: point set.
    :return: the order which is the numerically the best.
    """
    n = len(nodes) - 1
    order = np.arange(1, n+1, dtype=np.int32)
    lj = np.zeros(n + 1, dtype=np.int32)
    lj[0] = 0
    m = 0
    for k in range(0, n):
        jj = 0
        for i in range(0, n - k):
            p = np.prod([abs(nodes[lj[_]] - nodes[order[i]]) for _ in range(k + 1)])
            if p >= m:
                jj = i
                m = p
        m = 0
        lj[k + 1] = order[jj]
        order = np.delete(order, jj)
    return lj


# UTILITIES FOR REGRESSION MATRICES #

def mui_lp_ext(n, p):
    """
    Extension of l^p-degree Multiindex-Set.

    Takes the conventional multiindex set A_{m, max(n), p} and deletes in every direction until the multi-index has
    a maximum degree for i-th component of n_i.

    :param n: a list of degrees.
    :param p: a value in [0, infinity]
    :return: the extended multi index set A_{(n_1, n_2, ..., n_m), p}.
    """
    if np.isscalar(n):
        return None

    m = len(n)
    mui = mui_lp(m, np.max(n), p)
    r = 0
    for i, item in enumerate(mui):
        for j, deg in enumerate(item):
            if deg > n[j]:
                mui = np.delete(mui, i - r, 0)
                r += 1
    return mui


def data_1d(x, deg):
    """
    Regression Matrix in the One-Dimensional Case w.r.t. the Chebyshev Basis.

    :param x: evaluation point set.
    :param deg: degree.
    :return: regression matrix in the one-dimensional case.
    """
    x = np.array(x)
    data = np.zeros((deg+1, len(x)))
    for _ in np.arange(0, deg+1):
        data[_] = eval_chebyt(_, x)
    return data


def data_axis(x, m):
    """
    Regression Matrix in the m-Dimensional Case w.r.t. the Chebyshev Basis.

    :param x: evaluation point set.
    :param m: dimension.
    :return: regression matrix in the m-dimensional case.
    """
    return data_axes([x for _ in range(m)], mui_lp(m, len(x), np.inf))


def data_axes(xs, mui):
    """
    Regression Matrix in the m-Dimensional Case w.r.t. the Chebyshev Basis.

    :param xs: list of lists of evaluation point sets.
    :param mui: multi-index set.
    :return: regression matrix in the len(xs)-dimensional case on the multi-index set.
    """
    deg = len(mui)
    m = len(xs)
    n_xs = [len(_) for _ in xs]
    n_flat = np.prod(n_xs)
    max_mui = int(np.max(mui))
    data_xs = [data_1d(__, max_mui) for _, __ in enumerate(xs)]
    data = np.zeros((deg, n_flat))
    for i, _ in enumerate(mui):
        data[i] = np.array(outer_arr(np.array([data_xs[(m-1)-__][_[(m-1)-__]] for __ in range(m)])))
    return data

# UTILITIES FOR MULTI GRAPH NORM #


def laplacian(operators, metric1, metric2, lam=1.0, weak=False, tensor=False):
    """
    Computation of Laplacian Operators.

    :param operators: operators.
    :param metric1: metric1.
    :param metric2: metric2.
    :param lam: dumping factor of the regularisation term.
    :param weak: if true => take Operator times Operator^* else => take Operator^* times Operator.
    :param tensor: if true => use torch functions else => use numpy functions.
    :return: the Laplacian operator w.r.t. the pair (<metric1*x, x>_2, <metric2*x, x>_2).
    """
    if isinstance(operators, list):
        if tensor:
            operators = torch.tensor(operators)
        else:
            operators = np.array(operators)

    if isinstance(metric1, list):
        if tensor:
            metric1 = torch.tensor(metric1)
        else:
            metric1 = np.array(metric1)

    if isinstance(metric2, list):
        if tensor:
            metric2 = torch.tensor(metric2)
        else:
            metric2 = np.array(metric2)

    if tensor:
        if (operators.size() != metric1.size()) | (operators.size() != metric2.size()):
            return None
    else:
        if (operators.shape != metric1.shape) | (operators.shape != metric2.shape):
            return None

    if tensor:
        if operators.size()[1] != operators.size[2]:
            return None
    else:
        if operators.shape[1] != operators.shape[2]:
            return None

    n = len(operators)

    I = None
    if tensor:
        I = torch.eye(operators[0].size()[0])
    else:
        I = np.eye(operators[0].shape[0])

    if weak:
        laplacian_inv = lam * I + sum(
            [matmul(operators[_], metric1[_], operators[_].T, metric2[_]) for _ in range(n)])
    else:
        laplacian_inv = lam * I + sum(
            [matmul(metric1[_], operators[_].T, metric2[_], operators[_]) for _ in range(n)])

    if tensor:
        return laplacian_inv.inverse(), laplacian_inv
    else:
        return inverse_lu(laplacian_inv), laplacian_inv


# UTILITIES FOR DIFFEOMORPHISMS #

def pullback(nabla, dphi, grid, tensor=False):
    """
    Pullback of Nabla.

    :param nabla: all first derivatives in a list - nabla.
    :param dphi: function which returns the jacobian matrix of the diffeomorphism phi at point x.
    :param grid: grid points, which are already pulled back by the diffeomorphism phi.
    :param tensor: if true => use torch functions else => use numpy functions.
    :return: the pullback of nabla.
    """
    if tensor:
        pb = [dphi(_).inverse() for _ in grid]
        pb = torch.cat(([_.reshape(1, *_.size()) for _ in pb]))
        return torch.matmul(nabla.T, pb).T
    else:
        pb = np.array([inverse_lu(dphi(_)) for _ in grid])
        return np.matmul(nabla.T, pb).T


# HYPER RECTANGULAR


def hyper_rect(ints, tensor=False):
    """
    Diffeomorphism of a hyper-rectangular

    :param ints: the hyperrectangular, as a list of intervals.
    :param tensor: if true => use torch functions else => use numpy functions.
    :return: the diffeomorphism as a function of input vectors v and the jacobian as a function of input vectors v.
    """
    midpoints = (ints[:, 1]-ints[:, 0])/2
    shifts = (ints[:, 1]+ints[:, 0])/2

    if tensor:
        return lambda v: [midpoints[_] * v[_] + shifts[_] for _ in range(len(ints))], lambda v: torch.diag(midpoints)
    else:
        return lambda v: np.array([midpoints[_]*v[_]+shifts[_] for _ in range(len(ints))]), lambda v: np.diag(midpoints)


# HYPER SPHERE #


def hyper_sphere(d):
    """
    Diffeomorphism of a Hyper-Sphere with Radius One.

    :param d: dimension.
    :return: the diffeomorphism as a function of input vectors v and the jacobian as a function of input vectors v.
    """
    return lambda v: np.array([sphere_term(v, _) for _ in range(d)]),\
        lambda v: np.array([sphere_term(v, _, grad=True) for _ in range(d)]).T


def prod_sin(v, i, j):
    """
    First Auxiliary Function for the Hyper Sphere Diffeomorphism.

    :param v:
    :param i:
    :param j:
    :return: auxiliary result.
    """
    pi = np.pi
    sin = lambda x: np.sin(x)
    return np.prod(sin(np.array([pi*(v[__]+1) for __ in range(i, j+1)])))


def sphere_term(v, i, grad=False):
    """
    Second Auxiliary Function for the Hyper Sphere Diffeomorphism.

    :param v:
    :param i:
    :param grad:
    :return: auxiliary result.
    """
    pi = np.pi
    sin = lambda x: np.sin(x)
    cos = lambda x: np.cos(x)

    if i == len(v)-1:
        prod = prod_sin(v, 1, len(v)-1)
        phi = 0.5*(v[0]+1)*prod

        if grad:
            dphi = np.zeros(len(v))
            dphi[0] = 0.5*prod
            for _ in range(1, len(v)):
                dphi[_] = phi*pi*cos(pi*(v[_]+1))/sin(pi*(v[_]+1))

    else:
        prod = prod_sin(v, 1, i)*cos(pi*(v[i+1]+1))
        phi = 0.5*(v[0]+1)*prod

        if grad:
            dphi = np.zeros(len(v))
            dphi[0] = 0.5*prod
            for _ in range(1, len(v)):
                if _ == len(v)-1:
                    dphi[_] = -phi*pi*sin(pi*(v[_]+1))/cos(pi*(v[_]+1))
                else:
                    dphi[_] = phi*pi*cos(pi*(v[_]+1))/sin(pi*(v[_]+1))

    if grad:
        return dphi
    else:
        return phi


# OPTIMISING #

def L(theta, pairs, prep=lambda x: x):
    """
    Loss function.

    :param theta: the input tensor.
    :param prep: is a mapping, which prepares theta before plugging in pairings.
    :param pairs:
        a two dimensional array of pairings of equations (first element) and their underlying metric tensors w.r.t. the
        euclidean inner product space (second element).
    :return:
        the loss, by summing of the evaluations weighted by the metrics in the euclidean inner product and the
        corresponding evaluations of the given equations.
    """
    u = prep(theta)
    evals = [[p[0](*u), p[1]] for p in pairs]
    loss = sum([sum(matmul(e[1], e[0])*e[0]) for e in evals])
    return loss, evals


def implicit_euler(theta_0, m, pairs, prep=None, N=50, tau=1.0, print_rate=10, print_grad=False):
    """
    An approximation to the implicit Euler method.

    :param theta_0: input.
    :param m: is the metric tensor, we transform the gradient to.
    :param prep: is a mapping, which prepares theta before plugging in pairings.
    :param pairs:
        a two dimensional array of parings of equations (first element) and their underlying metric tensors w.r.t.
        the euclidean inner product space (second element).
    :param N: amount of iterations.
    :param tau:
        implicit euler step ~ 1/tau. For tau, one can plug in a list of step sizes. They are treated, in the following
        way. The first index for tau is the (floored) midpoint of the array. If the loss gets worse with the current
        step size, it increases the index. But, if the loss gets better, it decreases the index. If the index is maximal
        and the loss still increases along the search direction, the algorithm terminates and gives the last computed
        value for theta back.
    :param print_rate: rate of prints.
    :param print_grad: decides, if the Frobenius norm of the gradient is also printed, in addition to the loss itself.
    :return: theta_N, after N iterations, which is an approximate solution to the optimisation problem.
    """

    # Length of input vector theta_0 and all the following iterations theta_n.
    l = len(theta_0)
    # Start with theta_n = theta_0:
    theta_n = theta_0
    if not torch.is_tensor(theta_n):
        theta_n = torch.tensor(theta_0)
    # set index for tau to the midpoint
    if (not isinstance(tau, list)) & np.isscalar(tau):
        tau = [tau]
    j = int(np.floor(len(tau)/2))
    # calculate loss at theta_n = theta_0
    theta_n.requires_grad = True
    last_loss, last_evals = L(theta_n, pairs, prep=prep)

    i = 0
    skip = False
    jac_n_q = None
    jac_n_s = None
    taus = np.array([])
    while i < N:

        with torch.no_grad():
            if not skip:
                # Use help function for the composition of the equations with the prepare function
                def f(func, prep):
                    return lambda theta: func(*prep(theta))
                funcs = [f(p[0], prep) for p in pairs]
                # Use autograd to obtain Jacobian matrices of each equation:
                jac_n = [torch.autograd.functional.jacobian(f, theta_n) for f in funcs]
                # Adjoint Jacobian matrices of each equation:
                jac_n_s = [matmul(m, matmul(p[1], jac_n[i]).T) for i, p in enumerate(pairs)]
                # Quadratic forms of Jacobians, i.e. J^T*J, for J Jacobian:
                jac_n_q = [matmul(jac_n_s[i], jac_n[i]) for i in range(len(jac_n))]

            # Preconditioner for an implicit Euler step:
            pre = torch.tensor(inverse_lu((tau[j]*torch.eye(l)+sum(jac_n_q)).detach().numpy()))
            # DON'T use autograd to obtain the gradient of the functional L, instead directly calculate:
            grad_n = 2*sum([matmul(jac_n_s[k], last_evals[k][0]) for k in range(len(pairs))])
            # Explicit Euler step with the implicit Euler preconditioner:
            theta = theta_n - 0.5*matmul(pre, grad_n)

        # Compute the loss afterwards:
        theta.requires_grad = True
        loss, evals = L(theta, pairs, prep=prep)

        with torch.no_grad():
            # Check if theta is decreasing the loss:
            if last_loss < loss:
                if j == len(tau)-1:
                    # Return theta_n, since it is not improving anymore.
                    break
                skip = True
                j += 1
            else:
                last_loss = loss
                last_evals = evals
                theta_n = theta
                taus = np.array([*taus, tau[j]])

                if (i + 1) % print_rate == 0:
                    if print_grad:
                        print(f'[{i + 1}]: loss={loss}, grad={torch.norm(grad_n)}')
                    else:
                        print(f'[{i + 1}]: {loss}')

                if j > 0:
                    j -= 1
                skip = False
                i += 1
    # Provide information about step size history:
    stats = {
        "iterations": i,
        "last_loss": last_loss.item(),
        "tau_min": np.min(taus),
        "tau_max": np.max(taus),
        "tau_median":np.median(taus)}
    # Return the theta_n after N iterations:
    return theta_n.detach().numpy(), stats


def prepare(*pairs):
    """
    Prepare function.

    :param pairs:
        a two dimensional array of pairs: first element should be the input matrix, second should be the amount of
        splits applied afterwards.
    :return: returns the right format for the prepare function of the implicit Euler method.
    """

    u_n = lambda X, theta: matmul(X, theta.reshape(len(theta), -1))
    return lambda theta: [x for p in pairs for x in torch.tensor_split(u_n(p[0], theta).T[0], p[1])]


# OTHERS #

def extract_models(theta, split):
    """
    Extracting Models from a Parameter Vector which contains All Models.

    :param theta: full parameter vector.
    :param split: modulus of splitting (positive integer).
    :return: in total split-different extracted models.
    """
    def helper(th):
        return lambda x: np.matmul(np.array(x), th.reshape(len(th), -1))

    return \
        [helper(theta) for theta in np.array_split(theta, split)]
