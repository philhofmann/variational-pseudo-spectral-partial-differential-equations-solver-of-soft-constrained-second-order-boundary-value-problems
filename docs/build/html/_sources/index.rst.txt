.. Variational Pseudo-Spectral Partial Differential Equations Solver of Soft-Constrained Second Order Boundary Value Problems documentation master file, created by
   sphinx-quickstart on Fri Dec  9 18:04:00 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Variational Pseudo-Spectral Partial Differential Equations Solver of Soft-Constrained Second Order Boundary Value Problems's documentation!
======================================================================================================================================================
This documentation includes thirteen experiments (notebooks), all required modules and classes, including also all private methods. For an in-depth explanation, please see my thesis :download:`Variational Pseudo-Spectral Partial Differential Equations Solver of Soft-Constrained Second Order Boundary Value Problems <thesis.pdf>`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   notebooks
   modules
   classes

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
