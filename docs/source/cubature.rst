cubature class
===============

.. autoclass:: cubature.Cubature
   :members: __init__, _axes, _leja_axes_bary, _leja_axes
   :undoc-members:
   :show-inheritance:
