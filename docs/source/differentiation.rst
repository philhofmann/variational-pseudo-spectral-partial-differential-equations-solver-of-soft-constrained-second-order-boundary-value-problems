differentiation class
======================

.. autoclass:: differentiation.Differentiation
   :members: __init__, _nabla, _dx, _shift, diffs
   :undoc-members:
   :show-inheritance:
