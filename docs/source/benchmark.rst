benchmark class
================

.. autoclass:: benchmark.Benchmark
   :members: __init__, set_model, eval_model, eval_gt, _plot1d, plot1d, _plot2d, plot2d, plot3d, plot_model, plot_gt, plot_abs_err, lp_err
   :undoc-members:
   :show-inheritance:
