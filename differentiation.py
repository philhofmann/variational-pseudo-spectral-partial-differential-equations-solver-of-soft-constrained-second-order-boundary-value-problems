import numpy as np
import itertools
from functools import reduce
from utilities import diff_mat, powers


class Differentiation:

    def __init__(self, axes, barycentric):
        """
        This is the Initialisation of the Differentiation Class.

        The differentiation matrices can be computed via the underlying point set and the corresponding barycentric
        weights.

        :param axes: list of each axis in each direction.
        :param barycentric: list of lists of corresponding barycentric weights.
        """
        self._dim = len(axes)
        self._sizes = [len(_) for _ in axes]
        self._size = int(np.prod(self._sizes))
        self._axes = axes
        self._barycentric = barycentric

        # computes the one dimensional differentiation matrices
        self._diff_mats = [diff_mat(self._axes[_], self._barycentric[_]) for _ in range(self._dim)]

        # transforms the list of one dimensional differentiation matrices to the tensorial ones.
        self.nabla = self._nabla()

    def _nabla(self):
        """
        Computes Nabla - the derivatives of each canonical direction in a list.

        :return: nabla.
        """
        d = self._dim
        sz = self._size
        dx = [self._dx(j) for j in range(d)]

        nabla = np.zeros((d, sz, sz))
        shifts = [self._shift(_+1) for _ in range(d-1)]

        nabla[0] = dx[0]
        for _ in range(d-1):
            nabla[_+1] = np.matmul(np.linalg.inv(shifts[_]), np.matmul(dx[_+1], shifts[_]))

        return nabla

    def _dx(self, j):
        """
        Auxiliary function.

        It computes the block diagonal matrix of the one dimensional differentiation matrix in
        the right manner. Meaning, we need to repeat the differentiation matrix until we reach len(mui) which is the
        length of the designated coordinate vector.

        :param j: denotes the axis index.
        :return: block diagonal matrix with len(mui)-times the matrix: _diff_mats[i].
        """
        n = self._sizes[j]
        sz = self._size
        diff_mat = self._diff_mats[j]

        mat = np.zeros((sz, sz))
        block_cart = np.array([_ for _ in itertools.product(range(int(sz / n)), range(n), range(n))])
        for _, __, ___ in block_cart:
            mat[_ * n + __][_ * n + ___] = diff_mat[__][___]

        return mat

    def _shift(self, j):
        """
        Auxiliary function.

        It computes the permutation matrices, which visually represent rotations, to obtain the tensorial
        differentiation matrices from the one dimensional ones.

        :param j: denotes the axis index.
        :return: permutation matrix.
        """
        mat = np.zeros((self._size, self._size))
        iter_len = int(np.prod(self._sizes[j:]))
        step_len = int(np.prod(self._sizes[:j]))

        k = 0
        for j in range(step_len):
            for i in range(iter_len):
                val = i * step_len + j
                mat[k][val] = 1
                k += 1
        return mat

    def diffs(self, mui):
        """
        Differentiation Matrix

        :param mui: multi-index of the differentiation matrix.
        :return: differentiation matrix for the multi-index mui.
        """
        d = self._dim
        sz = self._size

        diffs = np.zeros((len(mui), sz, sz))

        # Compute in every direction until the highest potencies which are necessary to build up the degree of mui.
        powers_nabla = [powers(self.nabla[_], np.max(mui[:, _])) for _ in range(d)]

        # Compute the differentiation matrix for the multi-index mui by composing the individual directions.
        for _, index in enumerate(mui):
            diffs[_] = reduce(np.dot, [powers_nabla[__][index[__]] for __ in range(d)])

        return diffs
