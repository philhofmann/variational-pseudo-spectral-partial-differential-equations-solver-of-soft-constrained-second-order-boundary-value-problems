import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from utilities import cart
from matplotlib import style

mpl.rcParams['lines.linewidth'] = 1.2
style.use("seaborn-pastel")

# partitions are just implemented in the y direction for dimension 2, all y-partitions need the same size.


class Benchmark:

    def __init__(self, gt, axes, data, y_partitioning=False, font_size=10, ticks_x=5, ticks_y=5, enlarge_x=1.0, enlarge_y=1.0):
        """
        This is the Initialisation of the Benchmark Class.

        One can initialise multiple Ground Truths, stored in a list.

        :param gt: the ground truth, which must passed as a function.
        :param axes: a list of lists of point sets, which will be used for testing errors.
        :param data: the data, which must be passed to the models - e.g. the regression matrices.
        :param y_partitioning: for dimension = 2, one can partitionate the y-axis.
        :param font_size: font size of plots.
        :param ticks_x: amount of ticks on the x-axis for 1d, 2d and 3d plots.
        :param ticks_y: amount of ticks on the y-axis for 2d and 3d plots.
        :param enlarge_x: factor which enlarges the plots in the x-direction.
        :param enlarge_y: factor which enlarges the plots in the y-direction.
        """
        self._gt = gt
        if not isinstance(gt, list):
            self._gt = [gt]

        self.axes = axes

        # compute number of partitions
        self.partitions_y = 0
        if y_partitioning:
            self.partitions_y = len(self.axes[1])

        self.data = data
        self.font_size = font_size
        self.enlarge_x = enlarge_x
        self.enlarge_y = enlarge_y
        self.ticks_x = ticks_x
        self.ticks_y = ticks_y

        # compute grid
        if self.partitions_y == 0:
            self.grid = cart(self.axes)
        else:
            self.grid = [cart([self.axes[0], self.axes[1][_]]) for _ in range(self.partitions_y)]

        # compute size of grid, only necessary if partitions_y == 0
        if self.partitions_y == 0:
            self.size = np.prod([len(_) for _ in self.axes])

        # compute concatenation of the y-axis partition
        if len(self.axes) >= 2:
            if self.partitions_y > 0:
                self.y_axis = np.concatenate(self.axes[1])
            else:
                self.y_axis = self.axes[1]

        # set model and evaluations initially to None
        self.model = None
        self._eval_model = None
        self._eval_gt = None

    def set_model(self, model):
        """
        Setter for self.model which guarantees integrity.

        One can initialise multiple Models, stored in a list.

        :param model: model, which must be passed at a function depending on the intialised data.
        """
        self.model = model

        if not isinstance(model, list):
            self.model = [model]

    def eval_model(self):
        """
        Evaluate Method for the passed Models.

        :return: all values of the evaluated models at the initialised data.
        """
        if self.model is not None:
            self._eval_model = [_(self.data).T[0] for _ in self.model]

            if self.partitions_y == 0:
                for i, _ in enumerate(self._eval_model):
                    if len(_) == self.size:
                        self._eval_model[i] = _.reshape(*[len(_) for _ in self.axes])
            else:
                for i, _ in enumerate(self._eval_model):
                    self._eval_model[i] = np.concatenate(
                        _.reshape(self.partitions_y, len(self.axes[0]), len(self.axes[1][0])))

            return self._eval_model
        return None

    def eval_gt(self):
        """
        Evaluate Method for the passed Ground Truth.

        :return: all values of the evaluated Ground Truths at the initialised testing axes.
        """
        if self._gt is not None:
            if self.partitions_y == 0:
                self._eval_gt = [
                    _(*[self.grid[:, i] for i in range(len(self.axes))]).reshape([len(__) for __ in self.axes])
                    for _ in self._gt]
            else:
                self._eval_gt = [
                    np.concatenate(np.array(
                        [_(*[self.grid[__][:, i] for i in range(len(self.axes))]).reshape(
                            len(self.axes[0]), len(self.axes[1][__])) for __ in range(self.partitions_y)]))
                    for _ in self._gt]

            return self._eval_gt
        return None

    def _plot1d(self, data, title="", file_name=None):
        """
        Private Plot Method for one Function which depends on One Variable.

        :param data: the values of the function evaluated at the initialised axes.
        :param title: title of the plot.
        :param file_name: default value is none, set to arbitrary name to safe the plot automatically.
        """
        return self.plot1d(
            data,
            self.axes[0],
            title,
            file_name=file_name,
            font_size=self.font_size,
            ticks_x=self.ticks_x,
            enlarge_x=self.enlarge_x,
            enlarge_y=self.enlarge_y)

    @staticmethod
    def plot1d(data, x, title="", file_name=None, font_size=10, ticks_x=5, enlarge_x=1.0, enlarge_y=1.0):
        """
        Static Plot Method for one Function which depends on One Variable.

        :param data: the values of the function evaluated at an arbitrary point set.
        :param x: point set where the function was evaluated.
        :param title: title of the plot.
        :param file_name: default value is none, set to arbitrary name to safe the plot automatically.
        :param font_size: font size of the plot.
        :param ticks_x: amount of ticks on the x-axis.
        :param enlarge_x: factor which enlarges the plots in the x-direction.
        :param enlarge_y: factor which enlarges the plots in the y-direction.
        """
        xticks = [_ for _ in range(len(x)) if _ % np.round(len(x) / (ticks_x - 1)) == 0]

        fig = plt.gcf()
        fig.set_size_inches(enlarge_x*5.5385, enlarge_y*3.15)
        plt.xticks(xticks, np.round(x[xticks], decimals=2), fontsize=font_size)
        plt.yticks(fontsize=font_size)
        plt.plot(data)
        plt.title(title, fontsize=font_size)

        if file_name is not None:
            fig.savefig(file_name+'.png', dpi=150)

        plt.show()

    def _plot2d(self, data, title="", file_name=None):
        """
        Private Plot Method for one Function which depends on Two Variable.

        :param data: the values of the function evaluated at the initialised axes.
        :param title: title of the plot.
        :param file_name: default value is none, set to arbitrary name to safe the plot automatically.
        """
        return self.plot2d(
            data,
            self.axes[0],
            self.y_axis,
            title,
            file_name=file_name,
            font_size=self.font_size,
            ticks_x=self.ticks_x,
            ticks_y=self.ticks_y,
            enlarge_x=self.enlarge_x,
            enlarge_y=self.enlarge_y)

    @staticmethod
    def plot2d(data, x, y, title="", file_name=None, font_size=10, ticks_x=5, ticks_y=5, enlarge_x=1.0, enlarge_y=1.0):
        """
        Static Plot Method for one Function which depends on Two Variables.

        :param data: the values of the function evaluated at an arbitrary two dimensional grid.
        :param x: point set where the function was evaluated in the first dimension.
        :param y: point set where the function was evaluated in the second dimension.
        :param title: title of the plot.
        :param file_name: default value is none, set to arbitrary name to safe the plot automatically.
        :param font_size: font size of the plot.
        :param ticks_x: amount of ticks on the x-axis.
        :param ticks_y: amount of ticks on the y-axis.
        :param enlarge_x: factor which enlarges the plots in the x-direction.
        :param enlarge_y: factor which enlarges the plots in the y-direction.
        """
        xticks = [_ for _ in range(len(x)) if _ % np.round(len(x) / (ticks_x - 1)) == 0]
        yticks = [_ for _ in range(len(y)) if _ % np.round(len(y) / (ticks_y - 1)) == 0]

        fig = plt.gcf()
        fig.set_size_inches(enlarge_x*5.5385, enlarge_y*3.15)
        plt.xticks(xticks, np.round(x[xticks], decimals=2), fontsize=font_size)
        plt.yticks(yticks, np.round(y[yticks], decimals=2), fontsize=font_size)
        plt.imshow(data, cmap="Spectral", origin="lower", aspect="auto")
        cbar = plt.colorbar()
        cbar.ax.tick_params(labelsize=font_size)
        plt.title(title, fontsize=font_size)

        if file_name is not None:
            fig.savefig(file_name+'.png', dpi=150)

        plt.show()

    @staticmethod
    def plot3d(data, t, x, y, file_name=None, font_size=16, ticks_x=5, ticks_y=5, enlarge_x=1.0, enlarge_y=1.0):
        """
        Static Plot Method for one Function which depends on Three Variables.

        Notice, that for every given time in t, there will occur on plot.

        :param data: the values of the function evaluated at an arbitrary three dimensional grid.
        :param t: point set where the function was evaluated in time.
        :param x: point set where the function was evaluated in the first dimension.
        :param y: point set where the function was evaluated in the second dimension.
        :param file_name: default value is none, set to arbitrary name to safe the plot automatically.
        :param font_size: font size of the plot.
        :param ticks_x: amount of ticks on the x-axis.
        :param ticks_y: amount of ticks on the y-axis.
        :param enlarge_x: factor which enlarges the plots in the x-direction.
        :param enlarge_y: factor which enlarges the plots in the y-direction.
        """
        xticks = [_ for _ in range(len(x)) if _ % np.round(len(x) / (ticks_x - 1)) == 0]
        yticks = [_ for _ in range(len(y)) if _ % np.round(len(y) / (ticks_y - 1)) == 0]

        rows = int(np.ceil(len(t)/5))
        fig = plt.figure(figsize=(enlarge_x*38, enlarge_y*12))

        for i in range(len(t)):
            fig.add_subplot(rows, 5, i+1)
            plt.xticks(xticks, np.round(x[xticks], decimals=2), fontsize=font_size)
            plt.yticks(yticks, np.round(y[yticks], decimals=2), fontsize=font_size)
            plt.imshow(data[i], cmap="Spectral", origin="lower", aspect="auto")
            cbar = plt.colorbar()
            cbar.ax.tick_params(labelsize=font_size)
            plt.title(f't={t[i]:.2f}', fontsize=font_size)

        if file_name is not None:
            fig.savefig(file_name + '.png', dpi=200)

        plt.show()

    def plot_model(self, prefix=None):
        """
        Model Plot Method

        :param prefix: plots will be save under the filename prefix+filename
        """
        if self._eval_model is not None:
            fn = None
            if prefix is not None:
                fn = prefix + "_model_plot_"

            dim = len(self.axes)
            for i, _ in enumerate(self._eval_model):
                fn_i = None
                if fn is not None:
                    fn_i = f'{fn}{i+1}'
                getattr(self, '_plot'+str(dim)+'d')(_, title=f'Model Plot {i+1}', file_name=fn_i)

    def plot_gt(self, prefix=None):
        """
        Ground Truth Plot Method

        :param prefix: plots will be save under the filename prefix+filename
        """
        if self._eval_gt is not None:
            fn = None
            if prefix is not None:
                fn = prefix + "_gt_plot_"

            dim = len(self.axes)
            for i, _ in enumerate(self._eval_gt):
                fn_i = None
                if fn is not None:
                    fn_i = f'{fn}{i+1}'
                getattr(self, '_plot' + str(dim) + 'd')(_, title=f'Ground Truth Plot {i+1}', file_name=fn_i)

    def plot_abs_err(self, prefix=None):
        """
        Absolute Error Plots Method

        :param prefix: plots will be save under the filename prefix+filename
        """
        if (self._eval_gt is not None) & (self._eval_model is not None):
            fn = None
            if prefix is not None:
                fn = prefix + "_abs_err_plot_"

            data = [abs(self._eval_gt[_ % len(self._gt)]-self._eval_model[_]) for _ in range(len(self.model))]

            dim = len(self.axes)
            for i, _ in enumerate(data):
                fn_i = None
                if fn is not None:
                    fn_i = f'{fn}{i+1}'
                getattr(self, '_plot' + str(dim) + 'd')(_, title=f'Absolute Error Plot {i+1}', file_name=fn_i)

    def lp_err(self, p=np.inf, precision=None):
        """
        l^p Error Method

        :param p: an arbitrary real number in [0, infinity]
        :param precision: the precision of the scientific format of numbers.
        :return: a list of l^p errors of the initiliased models to the initialised ground truths.
        """
        format = lambda x: x
        if not precision is None:
            format = lambda x: np.format_float_scientific(x, precision=precision)

        if (self._eval_gt is not None) & (self._eval_model is not None):
            if p == np.inf:
                return [format(
                    np.max(abs(self._eval_gt[_ % len(self._gt)]-self._eval_model[_]))
                ) for _ in range(len(self.model))]
            else:
                return [format(
                    np.mean((abs(self._eval_gt[_ % len(self._gt)]-self._eval_model[_]))**p)**(1/p)
                ) for _ in range(len(self.model))]
